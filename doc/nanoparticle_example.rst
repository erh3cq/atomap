.. _nanoparticle_example:

====================
Nanoparticle example
====================

See the notebook in the Atomap-demos repository: https://gitlab.com/atomap/atomap_demos/blob/release/nanoparticle_example_notebook/nanoparticle_example.ipynb
